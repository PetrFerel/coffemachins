package main

import p "CoffeeMachine/LogicMachin"

func main() {
	menuChoose := p.ChooseMenu{
		Latte:     100,
		Capuchino: 110,
		Machiato:  89,
	}
	p.Drink(menuChoose)
}
