package LogicMachin

type CoffeMenu interface {
	GetDrink() // Get your drink
}

func Drink(menu CoffeMenu) { // Method Implementation interfaces
	menu.GetDrink()
}

type ChooseMenu struct {
	Latte     int
	Machiato  int
	Capuchino int
	NameDrink string
}
type Cost struct {
	Price int
}
