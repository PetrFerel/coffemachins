package LogicMachin

import (
	"fmt"
	"log"
	"os"
	"time"
)

func (c ChooseMenu) GetDrink() {
	fmt.Println("Hello!\n", "Today:", time.Now().Weekday())
	var menu = 0
	fmt.Print("Choose Your Drink Please ! :\n" +
		" Latte:(1)" +
		" Capuchino:(2)" +
		" Machiato:(3)\n")

	if _, err := fmt.Scanln(&menu); err != nil {
		fmt.Fprint(os.Stderr, "Вы ввели не  число !\n")
	}
	switch menu {
	case 1:
		SwitchLogicCoffeMenuLatte(c)

	case 2:

		SwitchLogicCoffeMenuCapuchino(c)
	case 3:

		SwitchLogicCoffeMenuMachiato(c)

	}
	log.Default()
}
